package main

import (
	"fmt"
	"os"
	"server/routes"

	"github.com/joho/godotenv"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

// invoked before main
func init() {
	if err := godotenv.Load(); err != nil {
		fmt.Println("No .env file found")
	}
}

func main() {

	// Get port from env variable
	port, portExists := os.LookupEnv("PORT")

	// Default fallback
	if !portExists {
		port = "8000"
	}

	// Connect to redis
	// db.Setup()

	// Router setup
	router := gin.New()
	router.Use(gin.Logger())
	router.Use(cors.Default())

	/**
	* 	Endpoints
	 */

	// Ping for testing
	router.GET("ping", routes.Ping)

	// Query our topic.
	router.POST("/topic", routes.TopicQuery)

	// this runs the server and allows it to listen to requests.
	router.Run(":" + port)
}
