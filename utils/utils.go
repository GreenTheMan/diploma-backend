package utils

import (
	"regexp"
	"server/models"
	"strconv"
	"strings"
)

// Redis hash to Go struct
func HashToStructResponse(hash *map[string]string) (*models.TopicResponse, error) {
	// Storage variables
	structResponse := models.TopicResponse{}
	var err error
	var temp float64

	//
	// Conversion of hash to struct
	//

	// Topic - string
	structResponse.Topic = (*hash)["Topic"]

	// Positive - int
	structResponse.Positive, err = strconv.Atoi((*hash)["Positive"])
	if err != nil {
		return nil, err
	}

	// Neutral - int
	structResponse.Neutral, err = strconv.Atoi((*hash)["Neutral"])
	if err != nil {
		return nil, err
	}

	// Negative - int
	structResponse.Negative, err = strconv.Atoi((*hash)["Negative"])
	if err != nil {
		return nil, err
	}

	// Count - int
	structResponse.Count, err = strconv.Atoi((*hash)["Count"])
	if err != nil {
		return nil, err
	}

	// AvgScore - float32
	temp, err = strconv.ParseFloat((*hash)["AvgScore"], 32)
	structResponse.AvgScore = float32(temp)
	if err != nil {
		return nil, err
	}

	return &structResponse, nil
}

// Twitter data preprocessing for NLP
func CleanTweet(s string) string {

	// Remove keywords
	s = removeKeywords(s)

	// Remove urls
	// s = removeUrls(s)

	// // Make sure it's not longer than 1000 characters, because of google api pricing.
	// s = truncateText(s, 999)

	return s
}

// Remove twitter keywords (retweet, etc)
func removeKeywords(s string) string {
	// Remove retweet prefix if present
	if strings.HasPrefix(s, "RT @") {
		retweet, _ := regexp.Compile("RT @.*?: ")
		s = retweet.ReplaceAllString(s, "")
	}
	return s
}

// // Detect and remove urls from string
// func removeUrls(s string) string {
// 	links, err := regexp.Compile(" ?(f|ht)(tp)(s?)(://)(.*)[.|/]([^ $]*)")
// 	if err != nil {
// 		fmt.Println("RemoveUrls error " + err.Error())
// 	}
// 	return links.ReplaceAllString(s, "")
// }

// // Truncates string s to be shorter than max characters. Doesn't change if the string is less than the specified max limit.
// func truncateText(s string, max int) string {
// 	if max > len(s) {
// 		return s
// 	}
// 	return s[:strings.LastIndex(s[:max], " ")]
// }
