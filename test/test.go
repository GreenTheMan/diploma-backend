package main

import (
	"context"
	"fmt"
	redisservice "server/db"
	"server/models"

	"github.com/joho/godotenv"
	"github.com/mitchellh/mapstructure"
)

// invoked before main
func init() {
	if err := godotenv.Load(); err != nil {
		fmt.Println("No .env file found")
	}
}

var ctx = context.Background()

func main() {
	redisClient := redisservice.GetClientInstance()
	cmd, err := redisClient.HGetAll(ctx, "Johnny Depp").Result()
	fmt.Println("key", cmd, err)
	// Prepare decoding of hash to topicResponse struct
	responseData := &models.TopicResponse{}
	mapstructure.Decode(cmd, &responseData)
	fmt.Println(*responseData)

	_, err = redisClient.Get(ctx, "soething").Result()
	if err != nil {
		fmt.Printf(err.Error())
	}
}
