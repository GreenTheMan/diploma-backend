package routes

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	sentimentservice "server/api/sentiment"
	twitterservice "server/api/twitter"
	redisservice "server/db"
	"server/models"
	"server/utils"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
)

var ctx = context.Background()

// Test GET request to /ping/
func Ping(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"message": "pong",
	})
}

// Method to handle POST requests to /topic/
func TopicQuery(c *gin.Context) {

	//
	//	Process the request
	//

	// Request body model object
	var requestBody models.TopicRequestBody

	// Bind json to request body object
	if err := c.BindJSON(&requestBody); err != nil {
		_ = fmt.Errorf(err.Error())
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	fmt.Println("--------------- Request Received ----------------")

	//
	// Check database for query
	//

	// Get singleton DB client connection
	redisClient := redisservice.GetClientInstance()

	// Search in redis for query term
	hGetAllCmd := redisClient.HGetAll(ctx, requestBody.Query)
	redisResult, _ := hGetAllCmd.Result()
	redisErr := hGetAllCmd.Err()
	fmt.Println(redisResult)
	if redisErr != nil {
		fmt.Println("ERR - Redis HGetAll call returned an error. Error msg: ", redisErr.Error())
	} else if len(redisResult) == 0 {
		fmt.Println("Query not found in Cache, continuing with normal operation...")
	} else {
		// Decode hash to a struct
		redisResponse, err := utils.HashToStructResponse(&redisResult)
		if err == nil {
			// If successfully found topic in redis
			fmt.Println("--------------- Found topic in Redis cache ----------------")
			c.JSON(http.StatusOK, gin.H{
				"result": *redisResponse,
			})
			return
		}
		fmt.Println("ERR - Redis hash to struct conversion returned an error. Error msg: ", err.Error())
	}

	//
	// Fetch tweets
	//

	tweets, err := twitterservice.SearchRecentTweets(requestBody.Query)
	if err != nil || len(tweets) <= 0 {
		fmt.Println(err.Error())
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"msg": "Error occured trying to fetch tweets",
		})
		return
	}

	fmt.Println("--------------- Tweets Fetched ----------------")

	//
	// Sentiment analysis on each tweet
	//

	apiResponse := models.TopicResponse{
		Topic: requestBody.Query,
	}
	var scoreSum float32

	// Printing relevant information
	hostname, hostExists := os.LookupEnv("SENTIMENT_SERVER_URL")
	if !hostExists {
		log.Fatal("Hostname or URL of sentiment server doesn't exist in env file")
	}
	fmt.Printf("Starting sentiment analysis to %s\n", hostname)

	// Using Golang's waitgroups to handle concurrency
	var wg sync.WaitGroup

	// Iteration over every tweet in array tweets
	for _, tweet := range tweets {
		// Tell waitgroup about 1 new concurrent process
		wg.Add(1)

		// Avoiding reuse of the same variable in each goroutine closure. See https://go.dev/doc/faq#closures_and_goroutines
		tweetText := tweet.Tweet.Text

		// Launch new goroutine
		go func() {
			// Get sentiment
			sentiment, err := sentimentservice.GetSentiment(tweetText)
			if err != nil {
				fmt.Println("Error analyzing tweet. err:" + err.Error())
				wg.Done()
				return
			}

			var sign float32

			// Categorize sentiment
			if sentiment.Label == "Positive" {
				apiResponse.Positive++
				sign = +1.0
			} else if sentiment.Label == "Negative" {
				apiResponse.Negative++
				sign = -1.0
			} else {
				apiResponse.Neutral++
				sign = 0.0
			}

			// Measurements for average score
			scoreSum += float32(sentiment.Score) * sign
			apiResponse.Count++

			// Signal that this goroutine is done to the waitgroup after finishing
			wg.Done()
		}()
	}

	// Wait for all concurrent goroutines to finish
	wg.Wait()

	apiResponse.AvgScore = scoreSum / float32(apiResponse.Count)

	// if count is too low
	if apiResponse.Count <= 5 {
		fmt.Println("Tweets analyzed is less then 5")
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"msg": "Insufficient tweets found",
		})
		return
	}

	fmt.Println("--------------- Sentiment analysis done ----------------")
	fmt.Println("Query: " + requestBody.Query + ", Response object: " + fmt.Sprintf("%#v", apiResponse))
	fmt.Println("--------------- Sentiment analysis done ----------------")

	// Save response object to Redis and set it to expire in 12 hours
	hsetCmd := redisClient.Do(
		ctx, "HSET", requestBody.Query,
		"Topic", apiResponse.Topic,
		"Positive", apiResponse.Positive,
		"Neutral", apiResponse.Neutral,
		"Negative", apiResponse.Negative,
		"Count", apiResponse.Count,
		"AvgScore", apiResponse.AvgScore,
	)
	_, hsetErr := hsetCmd.Result()
	if hsetErr != nil {
		fmt.Println("ERR: Redis error on HSET query. Error msg: " + hsetErr.Error())
	} else {
		// Set hash to expire in 12 hours.
		expireCmd := redisClient.Expire(ctx, requestBody.Query, time.Hour*12)
		_, expireErr := expireCmd.Result()
		if expireErr != nil {
			fmt.Println("ERR: Redis error on EXPIRE query. Error msg: " + expireErr.Error())
		}
	}

	// Respond to request
	c.JSON(http.StatusOK, gin.H{
		"result": apiResponse,
	})
}
