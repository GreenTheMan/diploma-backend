package twitterservice

import (
	"context"
	"fmt"
	"net/http"

	twitter "github.com/g8rswimmer/go-twitter/v2"
)

type authorize struct {
	Token string
}

func (a authorize) Add(req *http.Request) {
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", a.Token))
}

// Search recent tweets, uses twitter_client singleton to search, returns map of pointers to Tweet Dictionary, and error.
func SearchRecentTweets(query string) (map[string]*twitter.TweetDictionary, error) {
	// Get singleton instance of twitter client
	clientConfig, err := getClientInstance()
	if err != nil {
		return nil, err
	}

	// Get response from fetch
	tweetResponse, err := clientConfig.client.TweetRecentSearch(context.Background(), query, *clientConfig.opts)
	if err != nil {
		return nil, err
	}

	// Dictionaries map[string]
	return tweetResponse.Raw.TweetDictionaries(), nil
}
