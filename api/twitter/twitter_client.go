package twitterservice

import (
	"fmt"
	"net/http"
	"os"
	"strconv"
	"sync"

	twitter "github.com/g8rswimmer/go-twitter/v2"
)

// Goroutine lock
var lock = &sync.Mutex{}

// Client config struct, used as a singleton
type clientConfig struct {
	client *twitter.Client
	opts   *twitter.TweetRecentSearchOpts
}

// Singleton implementation for twitter client
var clientInstance *clientConfig

// Get singleton instance, create one if not initialized.
func getClientInstance() (*clientConfig, error) {
	if clientInstance == nil {
		lock.Lock()
		defer lock.Unlock()
		if clientInstance == nil {
			fmt.Println("Creating single instance now.")

			// check for env twitter bearer
			bearer, exists := os.LookupEnv("TWITTER_BEARER")
			if !exists {
				return nil, fmt.Errorf("environment: twitter authentication not provided. please add TWITTER_BEARER to your .env file")
			}

			// get max amount from env
			var maxTweets int
			maxTweetsString, exists := os.LookupEnv("MAX_TWEETS")
			if !exists {
				fmt.Println("MAX_TWEETS not provided, defaulting to 10")
				maxTweets = 10
			} else {
				var err error
				maxTweets, err = strconv.Atoi(maxTweetsString)
				if err != nil {
					fmt.Println("MAX_TWEETS error converting from str to int, defaulting to 10")
					maxTweets = 10
				}
			}

			// create new client
			new_client := twitter.Client{
				Authorizer: authorize{
					Token: bearer,
				},
				Client: http.DefaultClient,
				Host:   "https://api.twitter.com",
			}
			new_opts := twitter.TweetRecentSearchOpts{
				// Expansions:  []twitter.Expansion{twitter.ExpansionAuthorID},
				// TweetFields: []twitter.TweetField{twitter.TweetFieldText},
				MaxResults: maxTweets,
			}
			clientInstance = &clientConfig{
				client: &new_client,
				opts:   &new_opts,
			}
		}
	}

	return clientInstance, nil
}
