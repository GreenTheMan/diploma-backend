// Sample language-quickstart uses the Google Cloud Natural API to analyze the
// sentiment of "Hello, world!".
package sentimentservice

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"server/models"
)

// Function to get sentiment of parameter string, through google api
func GetSentiment(queryString string) (*models.SentimentResponse, error) {
	// get hostname of sentiment server from env file
	hostname, hostExists := os.LookupEnv("SENTIMENT_SERVER_URL")
	if !hostExists {
		log.Fatal("Hostname or URL of sentiment server doesn't exist in env file")
	}

	url := hostname + "/sentiment"

	httpClient := http.Client{}

	bodyStruct := models.SentimentRequest{
		Content: queryString,
	}

	bodyJson, err := json.Marshal(bodyStruct)
	if err != nil {
		log.Fatal(err)
	}

	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(bodyJson))
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		return nil, err
	}

	res, getErr := httpClient.Do(req)
	if getErr != nil {
		return nil, getErr
	}

	if res.Body != nil {
		defer res.Body.Close()
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		return nil, readErr
	}

	response := models.SentimentResponse{}
	jsonErr := json.Unmarshal(body, &response)
	if jsonErr != nil {
		return nil, jsonErr
	}

	return &response, nil
	// // Get singleton instance of clientConfig
	// clientConfig, err := getClientInstance()
	// if err != nil {
	// 	return nil, err
	// }

	// ctx := context.Background()

	// // Detects the sentiment of the text.
	// sentiment, err := clientConfig.client.AnalyzeSentiment(ctx, &languagepb.AnalyzeSentimentRequest{
	// 	Document: &languagepb.Document{
	// 		Source: &languagepb.Document_Content{
	// 			Content: queryString,
	// 		},
	// 		Type: languagepb.Document_PLAIN_TEXT,
	// 	},
	// 	EncodingType: languagepb.EncodingType_UTF8,
	// })
	// if err != nil {
	// 	return nil, err
	// }

	// // Return sentiment
	// return sentiment.DocumentSentiment, nil
}
