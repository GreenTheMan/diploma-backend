package redisservice

import (
	"fmt"
	"os"
	"sync"

	"github.com/go-redis/redis/v8"
)

// Goroutine lock
var lock = &sync.Mutex{}

// Singleton implementation for twitter client
var clientInstance *redis.Client

// Get singleton instance, create one if not initialized.
func GetClientInstance() *redis.Client {
	if clientInstance == nil {
		lock.Lock()
		defer lock.Unlock()
		if clientInstance == nil {
			fmt.Println("Creating single instance now.")

			// get url of redis server from env file
			url, urlExists := os.LookupEnv("REDIS_SERVER_URL")
			if !urlExists {
				fmt.Println("Redis server url not found in env file. Defaulting to localhost:6379")
				url = "localhost:6379"
			}

			clientInstance = redis.NewClient(&redis.Options{
				Addr:     url,
				Password: "", // no password set
				DB:       0,  // use default DB
			})
		}
	}

	return clientInstance
}
