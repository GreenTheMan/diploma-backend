package models

// Request HTTP body object sent to sentiment server
type SentimentRequest struct {
	Content string
}

// Response HTTP body object received from sentiment server
type SentimentResponse struct {
	Label string
	Score float64
}
