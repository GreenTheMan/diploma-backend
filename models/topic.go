package models

// Request HTTP body object that the server receives from the frontend
type TopicRequestBody struct {
	Query string
}

// Response HTTP body object that the server sends to the frontend
type TopicResponse struct {
	Topic    string
	Positive int
	Neutral  int
	Negative int
	Count    int
	AvgScore float32
}
